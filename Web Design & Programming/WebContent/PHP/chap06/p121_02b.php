<?php

$file = $_GET['target'];

echo file_exists($file) ? 'File exists' : 'File does not exist';

echo "<br /><br />";

echo is_executable($file) ? 'File is executable' : 'File is not executable';

echo "<br /><br />";

echo is_readable($file) ? 'File is readable' : 'File is not readable';

echo "<br /><br />";

echo is_writable($file) ? 'File is writeable' : 'File is not writeable';

echo "<br /><br />";

echo 'File size is ' . filesize($file) . ' bytes';

echo "<br /><br />";

echo fileowner($file);

echo "<br /><br />";

echo 'File type is ' . filetype($file);

?>