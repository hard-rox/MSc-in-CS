create table movies(
    mid int(10) unsigned not null auto_increment,
    mtitle varchar(255) not null default '',
    myear year(4) not null default '0000',
    primary key(mid)
) engine = MyISAM;


create table persons(
    pid int(11) unsigned not null auto_increment,
    pname varchar(255) not null default '',
    primary key(pid)
) engine = MyISAM;

create table roles(
    mid int(11) not null default '0',
    pid int(11) not null default '0',
    role enum('A','D') not null default 'A',
    primary key (mid, pid, role)
) engine = MyISAM;