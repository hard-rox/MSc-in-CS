<?php

// $flavors = array('strawberry','grape','vanilla','caramel','chocolate');

$flavors[0] = 'strawberry';
$flavors[1] = 'grape';
$flavors[2] = 'vanilla';
$flavors[3] = 'caramel';
$flavors[4] = 'chocolate';
$flavors[] = 'banana';
$flavors[] = 'mango';


echo ":: Array Output :: <br />";

echo $flavors[0]."<br />";
echo $flavors[1]."<br />";
echo $flavors[2]."<br />";
echo $flavors[3]."<br />";
echo $flavors[4]."<br />";
echo $flavors[5]."<br />";
echo $flavors[6]."<br />";

$flavors[0] = 'blueberry';

echo "<br /> :: Modified Array Output :: <br />";

echo $flavors[0]."<br />";
echo $flavors[1]."<br />";
echo $flavors[2]."<br />";
echo $flavors[3]."<br />";
echo $flavors[4]."<br />";
echo $flavors[5]."<br />";
echo $flavors[6]."<br />";

// $fruits = array('red'=>'apple','yellow'=>'banana','purple'=>'plum','green'=>'grape');

$fruits['red'] = 'apple';
$fruits['yellow'] = 'banana';
$fruits['purple'] = 'plum';
$fruits['green'] = 'grape';
$fruits['pink'] = 'peach';

echo "<br /> :: Associative Array ( or Hash Array ) Output :: <br />";

echo $fruits['red']."<br />";
echo $fruits['yellow']."<br />";
echo $fruits['purple']."<br />";
echo $fruits['green']."<br />";
echo $fruits['pink']."<br />";

?>

