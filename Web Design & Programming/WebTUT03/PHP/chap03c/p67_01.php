<html>

  <head>

    <basefont face="Arial">

    <title>This a PHP page .</title>

  </head>

    
  <body>
    

    <?php

     $auth = true; 
     $age = 27; 
     $name = 'Bobby'; 
     $temp = 98.6; 

     echo '$auth = ';
     echo "$auth :: ";
     echo gettype($auth); //returns type "boolean"
     echo ' :: ';
     echo is_bool($auth);
     echo '<br />';

     echo '$age = '; 
     echo "$age :: ";
     echo gettype($age); //returns type "integer"
     echo ' :: ';
     echo is_int($age);
     echo '<br />';

     settype($age, 'double');
     echo '$age = '; 
     echo "$age :: ";
     echo gettype($age); //returns type "double"
     echo ' :: ';
     echo is_double($age);
     echo '<br />';

     echo '$name = ';
     echo "$name :: ";
     echo gettype($name); //returns type "string"
     echo ' :: ';
     echo is_string($name);
     echo '<br />';

     echo '$temp = ';
     echo "$temp :: ";
     echo gettype($temp); //returns type "double"
     echo ' :: ';
     echo is_double($temp);
     echo '<br />'; 


    ?>

  </body>


</html>