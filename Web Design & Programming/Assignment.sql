create database jumpstart;

use jumpstart;

create table employee(
    employee_id integer unsigned not null auto_increment,
    last_name varchar(30) not null,
    first_name varchar(30) not null,
    email varchar(100) not null,
    hire_date date not null,
    notes mediumtext,

    primary key(employee_id),
    index(last_name),
    unique(email)
)
engine=InnoDB;

create table address(
    employee_id integer unsigned not null,
    address varchar(50) not null,
    city varchar(30) not null,
    state char(2) not null,
    post_code char(5) not null,

    foreign key(employee_id)
        references employee(employee_id)
)
engine=InnoDB;

-- View the table design.
describe employee; 

-- Generate create table statement.
show create table employee;

create table charset_example(
    id integer unsigned not null auto_increment,
    ascii_string varchar(255) character set ascii not null,
    latin1_string varchar(255) character set latin1 not null,
    utf8_string varchar(255) character set utf8 not null,
    primary key(id)
) engine=InnoDB;

insert into employee (first_name, last_name, email, hire_date)
values ('Rasedur', 'Rahman', 'roxy.xprez@gmail.com', '2018-05-24');

select * from employee;

--There is a jhamela...
insert into employee (null, 'Last', 'Mr. First', 'a@b.com', '2018-05-24', null);

insert into address 
    (employee_id, address, city, state, post_code)
values
    (1, 'Dhaka', 'Dhaka', 'DK', '12345');

start transaction;
INSERT INTO employee (last_name, first_name, email, hire_date)
VALUES ('Virey', 'Gener', 'gvirey@example.com', '2015-04-02');
select * from employee;

rollback;
select * from employee;

start transaction;
INSERT INTO employee(last_name, first_name, email, hire_date)
VALUES('Virey', 'Gener', 'gvirey@example.com', '2015-04-02');
INSERT INTO address(employee_id, address, city, state, post_code)
VALUES(5, '227 North Avenue', 'Anytowne', 'XE', '97052');
commit;


--Chpter 3
--sakila-db

select first_name, last_name from actor;

select first_name, last_name from actor order by last_name;

select last_name, first_name from actor order by last_name asc, first_name desc;

show collation;

select last_name, first_name from actor order by last_name collate utf8_bin asc;

select first_name, last_name from actor limit 5;

select title, length, rating from film where length < 60 order by title;

select title, length, rating from film where length >= 60 and length <= 120 order by title;

select title, length, rating from film where length = 60 or length = 120 order by title;