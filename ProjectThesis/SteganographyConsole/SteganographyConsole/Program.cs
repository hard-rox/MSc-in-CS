﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SteganographyConsole
{
    class WavFile
    {
        public byte[] RiffId; // "riff"
        public uint Size;  // Size
        public byte[] WavId;  // Format
        public byte[] FmtId;  // Subchunk1ID
        public uint FmtSize; // Subchunk size
        public ushort Format; // format
        public ushort Channels; // no of channels
        public uint SampleRate; // Samplerate
        public uint BytePerSec;
        public ushort BlockSize;
        public ushort Bit;
        public byte[] DataId;// "data"
        public uint DataSize;
        public List<short> LeftStream;
        public List<short> RightStream;
        
        public BinaryReader Br;

        public WavFile(string filePath)
        {
            this.Br = new BinaryReader(new FileStream(filePath, FileMode.Open));

            this.RiffId = Br.ReadBytes(4);
            this.Size = Br.ReadUInt32();
            this.WavId = Br.ReadBytes(4);
            this.FmtId = Br.ReadBytes(4);
            this.FmtSize = Br.ReadUInt32();
            this.Format = Br.ReadUInt16();
            this.Channels = Br.ReadUInt16();
            this.SampleRate = Br.ReadUInt32();
            this.BytePerSec = Br.ReadUInt32();
            this.BlockSize = Br.ReadUInt16();
            this.Bit = Br.ReadUInt16();
            this.DataId = Br.ReadBytes(4);
            this.DataSize = Br.ReadUInt32();

            this.LeftStream = new List<short>();
            this.RightStream = new List<short>();
            for (var i = 0; i < this.DataSize / this.BlockSize; i++)
            {
                this.LeftStream.Add((short)Br.ReadUInt16());
                this.RightStream.Add((short)Br.ReadUInt16());
            }
            Br.Close();
        }

        public void WriteFile(string path)
        {
            this.DataSize = (uint)Math.Max(LeftStream.Count, RightStream.Count) * 4;

            var fs = new FileStream(path, FileMode.Create, FileAccess.Write);
            var bw = new BinaryWriter(fs);

            bw.Write(this.RiffId);
            bw.Write(this.Size);
            bw.Write(this.WavId);
            bw.Write(this.FmtId);
            bw.Write(this.FmtSize);
            bw.Write(this.Format);
            bw.Write(this.Channels);
            bw.Write(this.SampleRate);
            bw.Write(this.BytePerSec);
            bw.Write(this.BlockSize);
            bw.Write(this.Bit);
            bw.Write(this.DataId);
            bw.Write(this.DataSize);

            for (var i = 0; i < this.DataSize / this.BlockSize; i++)
            {
                if (i < this.LeftStream.Count)
                    bw.Write((ushort)this.LeftStream[i]);
                else
                    bw.Write(0);

                if (i < this.RightStream.Count)
                    bw.Write((ushort)this.RightStream[i]);
                else
                    bw.Write(0);
            }

            fs.Close();
            bw.Close();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Select:\n1. Hide\n2. Show");
            var menu = Console.ReadLine();

            if (menu == "1")
            {
                Console.WriteLine("Enter File Path");
                //C:\Users\RasedurRahman\Desktop\New folder\file_example_WAV_2MG.wav
                var filePath = Console.ReadLine();
                Console.WriteLine("Enter Messege: ");
                var messege = Console.ReadLine();

                Hide(filePath, messege);
                Console.WriteLine("Hiding Done.");
            }
            else
            {
                Console.WriteLine("Enter File Path");
                var filePath = Console.ReadLine();

                var messege = Extract(filePath);
                Console.WriteLine("Messege is: \n" + messege);
            }

            Console.ReadKey();
        }

        private static string Extract(string filePath)
        {
            var file = new WavFile(filePath);

            var leftStream = file.LeftStream;
            var rightStream = file.RightStream;

            var msgLength = 32767 * leftStream[0] + rightStream[0];
            var storageBlockLength = (int) msgLength / (leftStream.Count + rightStream.Count);
            var msgBytes = new List<byte>();

            for (var i = 0; i < leftStream.Count; i++)
            {
                if(msgBytes.Count == msgLength) break;
                msgBytes.Add((byte) leftStream[i]);
                msgBytes.Add((byte) rightStream[i]);
            }
            var msg = Encoding.UTF8.GetString(msgBytes.ToArray());
            return msg;
        }

        private static void Hide(string filePath, string messege)
        {
            var file = new WavFile(filePath);

            var leftStream = file.LeftStream;
            var rightStream = file.RightStream;
            var msgBytes = Encoding.UTF8.GetBytes(messege);

            var strorageBlockSize = (int) msgBytes.Length / (leftStream.Count + rightStream.Count);
            leftStream[0] = (short) (msgBytes.Length / 32767);
            rightStream[0] = (short)(msgBytes.Length % 32767);

            var msgIdx = 0;
            for (var i = 1; i < leftStream.Count; i++)
            {
                if(msgIdx >= msgBytes.Length) break;
                var tempByte = msgBytes[msgIdx++];
                leftStream.Insert(i, tempByte);
                tempByte = msgBytes[msgIdx++];
                rightStream.Insert(i, tempByte);
            }

            file.LeftStream = leftStream;
            file.RightStream = rightStream;

            file.WriteFile("Output" + Path.GetFileName(filePath));
        }
    }
}
